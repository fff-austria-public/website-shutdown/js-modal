# Website Shutdown f�r den 4. weltweiten Klimastreik - Installationsanleitung

Mit Hilfe dieses Skripts nimmt Ihre Website am 4. weltweiten Klimastreik teil.

## Inhaltsverzeichnis
* [Beschreibung](#beschreibung)
* [Vorschau](#vorschau)
* [Installation - Einfach](#installation-einfach)
    * [mit Google Analytics](#mit-google-analytics)
    * [ohne Google Analytics](#ohne-google-analytics)
* [Installation - Custom](#installation-custom)

## Beschreibung

An allen Daten vor dem 29.11.2019 wird ein Banner am unteren Rand des Browserfensters
angezeigt.

Am 29.11.2019 wird dieses Banner fensterf�llend,
d.h. die ganze Website wird mit der Aufforderung, zum Klimastreik zu kommen, �berlagert.

Der Nutzer hat die M�glichkeit standardm��ig immer die M�glichkeit das Banner
zu quittieren, sodass es nicht mehr sichtbar ist.
Diese Option kann deaktiviert werden, wenn gew�nscht.

Nach dem 29.11.2019 verschwindet das Banner komplett.


## Vorschau

### Vor dem 29.11.2019:

![Screenshot der Website vor dem 29.11.2019](https://i.imgur.com/KlJop6M.png)

### Am 29.11.2019:

![Screenshot der Website am 29.11.2019](https://i.imgur.com/RfitB9G.png)

### Nach dem 29.11.2019:

![Screenshot der Website nach dem 29.11.2019](https://i.imgur.com/gSQIGod.png)

## Installation - Einfach

### mit Google Analytics

Bei ihrer Website in die Datei **index.html** (am besten vor `</head>`) folgende Zeile hinzuf�gen:
```
<script src="https://fff-austria-public.gitlab.io/website-shutdown/js-modal/embed.js" async></script>
```

### ohne Google Analytics

Bei ihrer Website in die Datei **index.html** (am besten vor `</head>`) folgende Zeile hinzuf�gen:
```
<script src="https://fff-austria-public.gitlab.io/website-shutdown/js-modal/embed_withoutGA.js" async></script>
```

## Installation - Custom

1. Die Dateien **[index-de.html](index-de.html)** und **[widget.js](widget.js)** in denselben Ordner, wie _index.html_ bzw. _index.php_ (Startseite der Webseite) geben.

2. In die Datei **index.html** (am besten vor `</head>`) folgende Zeilen hinzuf�gen:

```
<script type="text/javascript">
    var DIGITAL_CLIMATE_STRIKE_OPTIONS = {
      cookieExpirationDays: 1,
      language: 'de',
      iframeHost: './index-de.html', // momentan gibt es nur deutschsprachigen Support. F�r eine englische Version wenden Sie sich bitte an den zust�ndigen Mitarbeiter von FridaysForFutureAustria
      disableGoogleAnalytics: false,
      alwaysShowWidget: false,
      forceFullPageWidget: false,
      showCloseButtonOnFullPageWidget: true, //Bitte schreiben Sie hier false statt true hin, wenn Sie wollen, dass das Banner am Tag des Streiks nicht geschlossen werden kann!
      footerDisplayStartDate: new Date(),
      fullPageDisplayStartDate: new Date(2019, 10, 29),
    };
  </script>
  <script src="widget.js" async></script>
```

3. Falls die M�glichkeit eines Close - Buttons am Tag des Streiks gew�nscht wird, das Wort `false` neben `showCloseButtonOnFullPageWidget:` durch das Wort `true` ersetzen. Darauf achten, dass der Beistrich nach `true` stehen bleibt!

4. Keine weiteren �nderungen durchf�hren, da damit die Funktionsweise des Programms beeintr�chtigt werden kann

## Authors

* **Andreas Ehrendorfer** - *Adjusting initial codebase* - [Andreas Ehrendorfer](https://gitlab.com/andreas_ehrendorfer/)
* **Max Fuxj�ger** - *Adjusting for single-line use and enabling hosting on GitLab* - [MaxValue](https://gitlab.com/MaxValue/)

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.

## Acknowledgments

* **Fight for the Future** - *Initial codebase* - [Project Page](https://github.com/fightforthefuture/digital-climate-strike)

### Project History
This project was created, because the outreach team of FFF Vienna (Austria) wanted
websites to be able to participate in the climate strike.
Max Fuxj�ger asked Andreas Ehrendorfer to adjust the code from _Fight for the Future_
to run again for the global climate strike on the 29.11.2019
